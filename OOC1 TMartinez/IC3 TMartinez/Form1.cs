﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOC1_TMartinez
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            double earthW = 0;
            double marsW = 0;
            try // added try catch so that only numerical values can be input
            {
                earthW = Double.Parse(textBox1.Text);
            }
            catch
            {
                MessageBox.Show("Error: Please enter a numerical value in the earth weight box.");
            }
            marsW = Math.Round((earthW / 9.81) * 3.711,3); // the math.round function will round to the 3rd decimal point.
            textBox2.Text = marsW.ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
