﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InClass4CST117
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
        }

            private void button1_Click(object sender, EventArgs e)
        {
            int seconds = 0;
            try
            {
                seconds = Int32.Parse(textBox1.Text);
            }
            catch
            {
                MessageBox.Show("Please Enter a Positive Integer" );
            }

            if (seconds >= 86400)
            {
                seconds = seconds / 86400;
                label3.Text = "Day(s): ";
                label4.Text = Convert.ToString(seconds);
            }
            else if (seconds >= 3600)
            {
                seconds = seconds / 3600;
                label3.Text = "Hour(s): ";
                label4.Text = Convert.ToString(seconds);
            }
            else if (seconds >= 60)
            {
                seconds = seconds / 60;
                label3.Text = "Minute(s): ";
                label4.Text = Convert.ToString(seconds);
            }
            else if (seconds < 60 && seconds >= 0)
            {
                label3.Text = "Second(s): ";
                label4.Text = Convert.ToString(seconds);
            }
            else
            {
                MessageBox.Show("Please Enter a Positive Integer");
            }
        }
    }
}
