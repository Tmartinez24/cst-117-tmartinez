﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OutClass2CST117
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                label1.Text = listBox1.Text;
            }
            else
            {

                label1.Text = Convert.ToString(changeToNum(listBox1.Text));
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
            if (checkBox1.Checked)
            {
                label3.Text = listBox1.Text;
            }
            else
            {

                label3.Text = Convert.ToString(changeToNum(listBox1.Text));
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int num1;
            int num2;

            if (checkBox1.Checked) //calculates the numbers and gives the result in the written form
            {
                num1 = changeToNum(label1.Text);
                num2 = changeToNum(label3.Text);

                if (radioButton1.Checked)
                {
                    label5.Text = changeToText(Convert.ToString(num1 + num2));
                }
                else if (radioButton2.Checked)
                {
                    label5.Text = changeToText(Convert.ToString(num1 - num2));
                }
                else if (radioButton3.Checked)
                {
                    label5.Text = changeToText(Convert.ToString(num1 * num2));
                }
                else if (radioButton4.Checked)
                {
                    label5.Text = changeToText(Convert.ToString(num1 / num2));
                }
                else //This line is in place for testing purposes
                {
                    MessageBox.Show("Error: Buttons not checked");
                }
            }
            else //calculates the numbers and gives the result in the numeral form
            {
                num1 = changeToNum(changeToText(label1.Text));
                num2 = changeToNum(changeToText(label3.Text));

                if (radioButton1.Checked)
                {
                    label5.Text = Convert.ToString(num1 + num2);
                }
                else if (radioButton2.Checked)
                {
                    label5.Text = Convert.ToString(num1 - num2);
                }
                else if (radioButton3.Checked)
                {
                    label5.Text = Convert.ToString(num1 * num2);
                }
                else if (radioButton4.Checked)
                {
                    label5.Text = Convert.ToString(num1 / num2);
                }
                else //This line is in place for testing purposes
                {
                    MessageBox.Show("Error: Buttons not checked");
                }
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked) //converts the text to the written form when the checkbox is true
            {
                label1.Text = changeToText(label1.Text);
                label3.Text = changeToText(label3.Text);
                label5.Text = changeToText(label5.Text);
            }
            else //converts the text to the numeral form when the checkbox is false
            {

                label1.Text = Convert.ToString(changeToNum(label1.Text));
                label3.Text = Convert.ToString(changeToNum(label3.Text));
                label5.Text = Convert.ToString(changeToNum(label5.Text));
            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            label2.Text = "+";
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            label2.Text = "-";
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            label2.Text = "*";
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            label2.Text = "/";
        }

        private string changeToText(string num)
        {
            string number = "";

            if (num == "1")
            {
                number = "One";
            }
            else if (num == "2")
            {
                number = "Two";
            }
            else if (num == "3")
            {
                number = "Three";
            }
            else if (num == "4")
            {
                number = "Four";
            }
            else if (num == "5")
            {
                number = "Five";
            }
            else if (num == "6")
            {
                number = "Six";
            }
            else if (num == "7")
            {
                number = "Seven";
            }
            else if (num == "8")
            {
                number = "Eight";
            }
            else if (num == "9")
            {
                number = "Nine";
            }
            else if (num == "0")
            {
                number = "Zero";
            }
            else if (num == "-1")
            {
                number = "Neg One";
            }
            else if (num == "-2")
            {
                number = "Neg Two";
            }

            return number;
        } //changes the labels to the written form of the number

        private int changeToNum(string num)
        {
            int number = 0;

            if (num == "One")
            {
                number = 1;
            }
            else if (num == "Two")
            {
                number = 2;
            }
            else if (num == "Three")
            {
                number = 3;
            }
            else if (num == "Four")
            {
                number = 4;
            }
            else if (num == "Five")
            {
                number = 5;
            }
            else if (num == "Six")
            {
                number = 6;
            }
            else if (num == "Seven")
            {
                number = 7;
            }
            else if (num == "Eight")
            {
                number = 8;
            }
            else if (num == "Nine")
            {
                number = 9;
            }
            else if (num == "Zero")
            {
                number = 0;
            }
            else if (num == "Neg One")
            {
                number = -1;
            }
            else if (num == "Neg Two")
            {
                number = -2;
            }

            return number;
        } //changes the labels to the numeral form of the number
    }
}
